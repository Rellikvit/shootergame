// Fill out your copyright notice in the Description page of Project Settings.


#include "ShooterGameStateEffect.h"
#include "Character/ShooterGameHealthComponent.h"
#include "Interface/ShooterGame_IGameActor.h"
#include "Kismet/GameplayStatics.h"
#include "Components/SkeletalMeshComponent.h"
#include "Engine/GameEngine.h"

bool UShooterGameStateEffect::InitObject(AActor* Actor)
{
	
	myActor = Actor;
	FVector MeshPosition;
	FName BoneName;

	IShooterGame_IGameActor* myInterface = Cast<IShooterGame_IGameActor>(myActor);
	if (myInterface)
	{
		myInterface->AddEffect(this);
		MeshPosition = myInterface->GetMeshOffset();
		BoneName = myInterface->GetSocketName();
	}
	if (ParticleEffect)
	{
		ParticleEmitter = UGameplayStatics::SpawnEmitterAttached(ParticleEffect, myActor->GetRootComponent(), BoneName, MeshPosition, FRotator::ZeroRotator, EAttachLocation::SnapToTarget, false);
		ParticleEmitter->SetRelativeTransform(ParticleTransform);
	}
	return true;
}

void UShooterGameStateEffect::DestroyObject()
{
	IShooterGame_IGameActor* myInterface = Cast<IShooterGame_IGameActor>(myActor);
	if (myInterface)
	{
		myInterface->RemoveEffect(this);
	}
	if (ParticleEmitter)
	{
		ParticleEmitter->DestroyComponent();
		ParticleEmitter = nullptr;
	}
	myActor = nullptr;
	if (this && this->IsValidLowLevel())
	{
		this->ConditionalBeginDestroy();
	}	
}

bool UShooterGameStateEffect_ExecuteOnce::InitObject(AActor* Actor)
{
	Super::InitObject(Actor);
	ExecuteOnce();
	return true;
}

void UShooterGameStateEffect_ExecuteOnce::DestroyObject()
{
	Super::DestroyObject();
}

void UShooterGameStateEffect_ExecuteOnce::ExecuteOnce()
{
	if (myActor)
	{
		UShooterGameHealthComponent* myHealthComp = Cast<UShooterGameHealthComponent>(myActor->GetComponentByClass(UShooterGameHealthComponent::StaticClass()));
		if (myHealthComp)
		{
			myHealthComp->ChangeHealthValue(Power);
		}
	}	
	DestroyObject();
}

bool UShooterGameStateEffect_ExecuteTimer::InitObject(AActor* Actor)
{
	Super::InitObject(Actor);

	GetWorld()->GetTimerManager().SetTimer(TimerHandle_EffectTimer, this, &UShooterGameStateEffect_ExecuteTimer::DestroyObject, Timer, false);
	GetWorld()->GetTimerManager().SetTimer(TimerHandle_ExecuteTimer, this, &UShooterGameStateEffect_ExecuteTimer::Execute, RateTime, true);
	
	return true;
}

void UShooterGameStateEffect_ExecuteTimer::DestroyObject()
{
	Super::DestroyObject();
}

void UShooterGameStateEffect_ExecuteTimer::Execute()
{
	if (myActor)
	{	
		//UGameplayStatics::ApplyDamage(myActor,Power,nullptr,nullptr,nullptr);	
		UShooterGameHealthComponent* myHealthComp = Cast<UShooterGameHealthComponent>(myActor->GetComponentByClass(UShooterGameHealthComponent::StaticClass()));
		if (myHealthComp)
		{
			myHealthComp->ChangeHealthValue(Power);
		}
	}
}
