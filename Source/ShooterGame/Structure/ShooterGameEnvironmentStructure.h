// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interface/ShooterGame_IGameActor.h"
#include "StateEffects/ShooterGameStateEffect.h"
#include "ShooterGameEnvironmentStructure.generated.h"

UCLASS()
class SHOOTERGAME_API AShooterGameEnvironmentStructure : public AActor, public IShooterGame_IGameActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AShooterGameEnvironmentStructure();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;


	EPhysicalSurface GetSurfuceType() override;	
	FVector GetMeshOffset() override;
	FName GetSocketName() override;
	
	TArray<UShooterGameStateEffect*> GetAllCurrentEffects() override;
	void RemoveEffect(UShooterGameStateEffect* RemoveEffect)override;
	void AddEffect(UShooterGameStateEffect* newEffect)override;

	//Effect
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting")
	TArray<UShooterGameStateEffect*> Effects;
};
