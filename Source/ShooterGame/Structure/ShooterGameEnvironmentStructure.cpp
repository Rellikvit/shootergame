// Fill out your copyright notice in the Description page of Project Settings.


#include "ShooterGameEnvironmentStructure.h"
#include "Materials/MaterialInterface.h"
#include "PhysicalMaterials/PhysicalMaterial.h"

// Sets default values
AShooterGameEnvironmentStructure::AShooterGameEnvironmentStructure()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AShooterGameEnvironmentStructure::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AShooterGameEnvironmentStructure::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

EPhysicalSurface AShooterGameEnvironmentStructure::GetSurfuceType()
{
	EPhysicalSurface Result = EPhysicalSurface::SurfaceType_Default;
	UStaticMeshComponent* myMesh = Cast<UStaticMeshComponent>(GetComponentByClass(UStaticMeshComponent::StaticClass()));
	if (myMesh)
	{
		UMaterialInterface* myMaterial = myMesh->GetMaterial(0);
		if (myMaterial)
		{
			Result = myMaterial->GetPhysicalMaterial()->SurfaceType;
		}
	}
	return Result;
}
FVector AShooterGameEnvironmentStructure::GetMeshOffset()
{
	FVector Result = FVector(0);
	UStaticMeshComponent* myStaticMesh = Cast<UStaticMeshComponent>(GetComponentByClass(UStaticMeshComponent::StaticClass()));
	USkeletalMeshComponent* mySkeletalMesh = Cast<USkeletalMeshComponent>(GetComponentByClass(USkeletalMeshComponent::StaticClass()));
	if (myStaticMesh)
	{
		Result = myStaticMesh->GetComponentLocation();
	}
	else if (mySkeletalMesh)
	{
		Result = mySkeletalMesh->GetSocketLocation("ParticleLocation");
	}
	return Result;
}

FName AShooterGameEnvironmentStructure::GetSocketName()
{
	FName Result = "None";
	USkeletalMeshComponent* mySkeletalMesh = Cast<USkeletalMeshComponent>(GetComponentByClass(USkeletalMeshComponent::StaticClass()));
	if (mySkeletalMesh)
	{
		Result = mySkeletalMesh->GetSocketBoneName("ParticleLocation");
	}
	return FName();
}

TArray<UShooterGameStateEffect*> AShooterGameEnvironmentStructure::GetAllCurrentEffects()
{
	return Effects;
}

void AShooterGameEnvironmentStructure::RemoveEffect(UShooterGameStateEffect* RemoveEffect)
{
	Effects.Remove(RemoveEffect);
}

void AShooterGameEnvironmentStructure::AddEffect(UShooterGameStateEffect* newEffect)
{
	Effects.Add(newEffect);
}