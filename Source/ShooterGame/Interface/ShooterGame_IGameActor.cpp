// Fill out your copyright notice in the Description page of Project Settings.


#include "ShooterGame_IGameActor.h"

// Add default functionality here for any ITPS_IGameActor functions that are not pure virtual.

EPhysicalSurface IShooterGame_IGameActor::GetSurfuceType()
{
	return EPhysicalSurface::SurfaceType_Default;
}

FVector IShooterGame_IGameActor::GetMeshOffset()
{
	return FVector();
}

FName IShooterGame_IGameActor::GetSocketName()
{
	return FName();
}

TArray<UShooterGameStateEffect*> IShooterGame_IGameActor::GetAllCurrentEffects()
{
	TArray<UShooterGameStateEffect*> Effect;
	return Effect;
}

void IShooterGame_IGameActor::RemoveEffect(UShooterGameStateEffect* RemoveEffect)
{

}

void IShooterGame_IGameActor::AddEffect(UShooterGameStateEffect* newEffect)
{

}
