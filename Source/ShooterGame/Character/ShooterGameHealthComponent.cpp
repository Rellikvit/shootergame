// Fill out your copyright notice in the Description page of Project Settings.


#include "Character/ShooterGameHealthComponent.h"

// Sets default values for this component's properties
UShooterGameHealthComponent::UShooterGameHealthComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UShooterGameHealthComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...

}


// Called every frame
void UShooterGameHealthComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

float UShooterGameHealthComponent::GetCurrentHealth()
{
	return Health;
}

void UShooterGameHealthComponent::SetCurrentHealth(float NewHealth)
{
	Health = NewHealth;
}

void UShooterGameHealthComponent::ChangeHealthValue(float ChangeValue)
{
	ChangeValue = ChangeValue * CoefDamage;

	Health -= ChangeValue;

	if (Health > 100.0f)
	{
		Health = 100.0f;
	}
	else
	{
		if (Health < 0.0f)
		{
			OnDead.Broadcast();
			IsDead = true;
		}
	}

	OnHealthChange.Broadcast(Health, ChangeValue);
}
