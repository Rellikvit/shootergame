// Fill out your copyright notice in the Description page of Project Settings.


#include "Character/ShooterGamePlayerHealthComponent.h"


void UShooterGamePlayerHealthComponent::ChangeHealthValue(float ChangeValue)
{
	float CurrentDamage = ChangeValue * CoefDamage;

	if (Shield > 0.0f && ChangeValue > 0.0f)
	{
		ChangeShieldValue(ChangeValue);

		if (Shield < 0.0f)
		{
			//FX
			//UE_LOG(LogTemp, Warning, TEXT("UTPSCharacterHealthComponent::ChangeHealthValue - Sheild < 0"));
		}
	}
	else
	{
		Super::ChangeHealthValue(ChangeValue);
	}
}

float UShooterGamePlayerHealthComponent::GetCurrentShield()
{
	return Shield;
}

void UShooterGamePlayerHealthComponent::ChangeShieldValue(float ChangeValue)
{
	Shield -= ChangeValue;

	if (Shield > 100.0f)
	{
		Shield = 100.0f;
	}
	else
	{
		if (Shield < 0.0f)
			Shield = 0.0f;
	}

	if (GetWorld())
	{
		if (GetWorld()->GetTimerManager().IsTimerPending(TimerHandle_CoolDownShieldTimer))
		{
			GetWorld()->GetTimerManager().ClearTimer(TimerHandle_CoolDownShieldTimer);
		}
		GetWorld()->GetTimerManager().SetTimer(TimerHandle_CoolDownShieldTimer, this, &UShooterGamePlayerHealthComponent::CoolDownShieldEnd, CoolDownShieldRecoverTime, false);

		GetWorld()->GetTimerManager().ClearTimer(TimerHandle_ShieldRecoveryRateTimer);
	}

	OnShieldChange.Broadcast(Shield, ChangeValue);
}

void UShooterGamePlayerHealthComponent::CoolDownShieldEnd()
{
	if (GetWorld())
	{
		GetWorld()->GetTimerManager().SetTimer(TimerHandle_ShieldRecoveryRateTimer, this, &UShooterGamePlayerHealthComponent::RecoveryShield, ShieldRecoverRate, true);
	}
}

void UShooterGamePlayerHealthComponent::RecoveryShield()
{
	if (!IsDead)
	{
		float tmp = Shield;
		tmp = tmp + ShieldRecoverValue;
		if (tmp > 100.0f)
		{
			Shield = 100.0f;
			if (GetWorld())
			{
				GetWorld()->GetTimerManager().ClearTimer(TimerHandle_ShieldRecoveryRateTimer);
			}
		}
		else
		{
			Shield = tmp;
		}

		OnShieldChange.Broadcast(Shield, ShieldRecoverValue);
	}
}