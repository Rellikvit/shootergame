// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "ShooterGameCharacter.h"
#include "UObject/ConstructorHelpers.h"
#include "Camera/CameraComponent.h"
#include "Components/DecalComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/SpringArmComponent.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Materials/Material.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "Engine/World.h"
#include "Components/SceneComponent.h"
#include "Game/ShooterGameGameInstance.h"

AShooterGameCharacter::AShooterGameCharacter()
{
	// Set size for player capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Don't rotate character to camera direction
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Rotate character to moving direction
	GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;

	// Create a camera boom...
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->SetUsingAbsoluteRotation(true); // Don't want arm to rotate when character does
	CameraBoom->TargetArmLength = 800.f;
	CameraBoom->SetRelativeRotation(FRotator(-60.f, 0.f, 0.f));
	CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level

	// Create a camera...
	TopDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
	TopDownCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	TopDownCameraComponent->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	InventoryComponent = CreateDefaultSubobject<UShooterGameInventoryComponent>(TEXT("InventoryComponent"));
	if (InventoryComponent)
	{
		InventoryComponent->OnSwitchWeapon.AddDynamic(this, &AShooterGameCharacter::InitWeapon);
	}
	CharHealthComponent = CreateDefaultSubobject<UShooterGamePlayerHealthComponent>(TEXT("HealthComponent"));
	if (CharHealthComponent)
	{
		CharHealthComponent->OnDead.AddDynamic(this, &AShooterGameCharacter::CharDead);
	}
	// Activate ticking in order to update the cursor every frame.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;
}

void AShooterGameCharacter::Tick(float DeltaSeconds)
{
    Super::Tick(DeltaSeconds);
	if (CurrentCursor)
	{
		APlayerController* myPC = Cast<APlayerController>(GetController());
		if (myPC)
		{
			FHitResult TraceHitResult;
			myPC->GetHitResultUnderCursor(ECC_Visibility, true, TraceHitResult);
			FVector CursorFV = TraceHitResult.ImpactNormal;
			FRotator CursorR = CursorFV.Rotation();

			CurrentCursor->SetWorldLocation(TraceHitResult.Location);
			CurrentCursor->SetWorldRotation(CursorR);
		}
	}
	MovementTick(DeltaSeconds);
}

void AShooterGameCharacter::BeginPlay()
{
	Super::BeginPlay();


	if (CursorMaterial)
	{
		CurrentCursor = UGameplayStatics::SpawnDecalAtLocation(GetWorld(), CursorMaterial, CursorSize, FVector(0));
	}
}

void AShooterGameCharacter::SetupPlayerInputComponent(UInputComponent* NewInputComponent)
{
	Super::SetupPlayerInputComponent(InputComponent);

	NewInputComponent->BindAxis(TEXT("MoveForward"), this, &AShooterGameCharacter::InputAxisX);
	NewInputComponent->BindAxis(TEXT("MoveRight"), this, &AShooterGameCharacter::InputAxisY);

	NewInputComponent->BindAction(TEXT("FireEvent"), EInputEvent::IE_Pressed, this, &AShooterGameCharacter::InputAttackPressed);
	NewInputComponent->BindAction(TEXT("FireEvent"), EInputEvent::IE_Released, this, &AShooterGameCharacter::InputAttackReleased);
	NewInputComponent->BindAction(TEXT("ReloadEvent"), EInputEvent::IE_Released, this, &AShooterGameCharacter::TryReloadWeapon);

	NewInputComponent->BindAction(TEXT("SwitchNextWeapon"), EInputEvent::IE_Pressed, this, &AShooterGameCharacter::TrySwicthNextWeapon);
	NewInputComponent->BindAction(TEXT("SwitchPreviosWeapon"), EInputEvent::IE_Pressed, this, &AShooterGameCharacter::TrySwitchPreviosWeapon);

	NewInputComponent->BindAction(TEXT("AbilityAction"), EInputEvent::IE_Pressed, this, &AShooterGameCharacter::TryAbilityEnabled);

	NewInputComponent->BindAction(TEXT("DropCurrentWeapon"), EInputEvent::IE_Pressed, this, &AShooterGameCharacter::DropCurrentWeapon);

	/*TArray<FKey> HotKeys;
	HotKeys.Add(EKeys::One);
	HotKeys.Add(EKeys::Two);
	HotKeys.Add(EKeys::Three);
	HotKeys.Add(EKeys::Four);
	HotKeys.Add(EKeys::Five);
	HotKeys.Add(EKeys::Six);
	HotKeys.Add(EKeys::Seven);
	HotKeys.Add(EKeys::Eight);
	HotKeys.Add(EKeys::Nine);
	HotKeys.Add(EKeys::Zero);

	NewInputComponent->BindKey(HotKeys[1], IE_Pressed, this, &AShooterGameCharacter::TKeyPressed<1>);
	NewInputComponent->BindKey(HotKeys[2], IE_Pressed, this, &AShooterGameCharacter::TKeyPressed<2>);
	NewInputComponent->BindKey(HotKeys[3], IE_Pressed, this, &AShooterGameCharacter::TKeyPressed<3>);
	NewInputComponent->BindKey(HotKeys[4], IE_Pressed, this, &AShooterGameCharacter::TKeyPressed<4>);
	NewInputComponent->BindKey(HotKeys[5], IE_Pressed, this, &AShooterGameCharacter::TKeyPressed<5>);
	NewInputComponent->BindKey(HotKeys[6], IE_Pressed, this, &AShooterGameCharacter::TKeyPressed<6>);
	NewInputComponent->BindKey(HotKeys[7], IE_Pressed, this, &AShooterGameCharacter::TKeyPressed<7>);
	NewInputComponent->BindKey(HotKeys[8], IE_Pressed, this, &AShooterGameCharacter::TKeyPressed<8>);
	NewInputComponent->BindKey(HotKeys[9], IE_Pressed, this, &AShooterGameCharacter::TKeyPressed<9>);
	NewInputComponent->BindKey(HotKeys[0], IE_Pressed, this, &AShooterGameCharacter::TKeyPressed<0>);*/
}

void AShooterGameCharacter::InputAxisX(float value)
{
	AxisX = value;
}

void AShooterGameCharacter::InputAxisY(float value)
{
	AxisY = value;
}

void AShooterGameCharacter::InputAttackPressed()
{
	if (!CurrentWeapon->WeaponReloading)
	{
		AttackCharEvent(true);
	}
}

void AShooterGameCharacter::InputAttackReleased()
{
	AttackCharEvent(false);
}

void AShooterGameCharacter::MovementTick(float DeltaTime)
{
	if (bIsAlive)
	{
		AddMovementInput(FVector(1.0f, 0.0f, 0.0f), AxisX);
		AddMovementInput(FVector(0.0f, 1.0f, 0.0f), AxisY);
		APlayerController* MyController = UGameplayStatics::GetPlayerController(GetWorld(), 0);
		if (MyController)
		{
			if (!Sprinting)
			{
				if (!Turning)
				{
					FHitResult ResultHit;
					MyController->GetHitResultUnderCursorByChannel(ETraceTypeQuery::TraceTypeQuery6, false, ResultHit);
					UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), ResultHit.Location);
					float FindRotatorResult = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), ResultHit.Location).Yaw;
					SetActorRotation(FQuat(FRotator(0.0f, FindRotatorResult, 0.0f)));
				}
				APlayerController* myController = UGameplayStatics::GetPlayerController(GetWorld(), 0);
				FHitResult ResultHit;
				//myController->GetHitResultUnderCursorByChannel(ETraceTypeQuery::TraceTypeQuery6, false, ResultHit);// bug was here Config\DefaultEngine.Ini
				myController->GetHitResultUnderCursor(ECC_GameTraceChannel1, true, ResultHit);
				if (CurrentWeapon)
				{
					FVector Displacement = FVector(0);
					switch (MovementState)
					{
					case EMovementState::AimState:
						Displacement = FVector(0.0f, 0.0f, 160.0f);
						CurrentWeapon->ShouldReduceDispersion = true;
						break;
					case EMovementState::AimWalkState:
						CurrentWeapon->ShouldReduceDispersion = true;
						Displacement = FVector(0.0f, 0.0f, 160.0f);
						break;
					case EMovementState::WalkState:
						Displacement = FVector(0.0f, 0.0f, 120.0f);
						CurrentWeapon->ShouldReduceDispersion = false;
						break;
					case EMovementState::SprintingState:
						break;
					default:
						break;
					}
					CurrentWeapon->ShootEndLocation = ResultHit.Location + Displacement;
				}
			}
		}
	}
	
}

void AShooterGameCharacter::AttackCharEvent(bool bIsFiring)
{
	AWeaponDefault* myWeapon = nullptr;
	myWeapon = GetCurrentWeapon();
	if (bIsAlive)
	{
		if (myWeapon)
		{
			myWeapon->SetWeaponStateFire(bIsFiring);
			if (myWeapon->WeaponFiring == true)
				if (!Aiming)
				{
					Walking = true;
					ChangeMovementState();
				}
				else
				{
					Walking = true;
					Aiming = true;
					ChangeMovementState();
				}
			else
			{
				Walking = false;
				ChangeMovementState();
			}
		}
		else
			UE_LOG(LogTemp, Warning, TEXT("AShooteGameCharacter::AttackCharEvent - CurrentWeapon -NULL"));
	}
}

void AShooterGameCharacter::CharacterUpdate()
{
	float ResSpeed = 600;
	switch (MovementState)
	{
	case EMovementState::AimWalkState:
		ResSpeed = MovementSpeed.AimWalkSpeed;
		break;
	case EMovementState::AimState:
		ResSpeed = MovementSpeed.AimSpeed;
		break;
	case EMovementState::RunState:
		ResSpeed = MovementSpeed.RunSpeed;
		break;
	case EMovementState::WalkState:
		ResSpeed = MovementSpeed.WalkSpeed;
		break;
	case EMovementState::SprintingState:
		ResSpeed = MovementSpeed.SprintingSpeed;
		break;
	default:
		break;
	}
	GetCharacterMovement()->MaxWalkSpeed = ResSpeed;
}

void AShooterGameCharacter::ChangeMovementState()
{
	if (!Walking && !Sprinting && !Aiming)
	{
		MovementState = EMovementState::RunState;
		Walking = false;
		Aiming = false;
		Sprinting = false;
	}
	else if (!CurrentWeapon->WeaponReloading)
	{

		if (Sprinting)
		{
			Walking = false;
			Aiming = false;
			MovementState = EMovementState::SprintingState;
		}
		else
		{
			if (Walking && Aiming)
			{
				MovementState = EMovementState::AimWalkState;
			}
			else
			{
				if (Walking && !Aiming)
				{
					MovementState = EMovementState::WalkState;
				}
				else
				{
					MovementState = EMovementState::AimState;
				}
			}
		}
	}
	CharacterUpdate();

	AWeaponDefault* myWeapon = GetCurrentWeapon();
	if (myWeapon)
	{
		myWeapon->UpdateStateWeapon(MovementState);
	}
}

AWeaponDefault* AShooterGameCharacter::GetCurrentWeapon()
{
	return CurrentWeapon;
}

TArray<UShooterGameStateEffect*> AShooterGameCharacter::GetCurrentEffectsOnChar()
{
	return Effects;
}

EMovementState AShooterGameCharacter::GetMovementState()
{
	return MovementState;
}

UDecalComponent* AShooterGameCharacter::GetCursorToWorld()
{
	return CurrentCursor;
}

int32 AShooterGameCharacter::GetCurrentWeaponIndex()
{
	return CurrentIndexWeapon;
}

void AShooterGameCharacter::InitWeapon(FName IdWeaponName, FAddicionalWeaponInfo WeaponAdditionalInfo, int32 NewCurrentIndexWeapon)
{
	if (CurrentWeapon)
	{
		CurrentWeapon->Destroy();
		CurrentWeapon = nullptr;
	}

	UShooterGameGameInstance* myGI = Cast<UShooterGameGameInstance>(GetGameInstance());
	FWeaponInfo myWeaponInfo;
	if (myGI)
	{
		if (myGI->GetWeaponInfoByName(IdWeaponName, myWeaponInfo))
		{
			if (myWeaponInfo.WeaponClass)
			{
				FVector SpawnLocation = FVector(0);
				FRotator SpawnRotation = FRotator(0);

				FActorSpawnParameters SpawnParams;
				SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
				SpawnParams.Owner = this;
				SpawnParams.Instigator = GetInstigator();

				AWeaponDefault* myWeapon = Cast<AWeaponDefault>(GetWorld()->SpawnActor(myWeaponInfo.WeaponClass, &SpawnLocation, &SpawnRotation, SpawnParams));
				if (myWeapon)
				{
					FAttachmentTransformRules Rule(EAttachmentRule::SnapToTarget, false);
					myWeapon->AttachToComponent(GetMesh(), Rule, FName("WeaponHoldSocket"));
					CurrentWeapon = myWeapon;
					myWeapon->IdWeaponName = IdWeaponName;
					myWeapon->WeaponSetting = myWeaponInfo;
					myWeapon->WeaponInfo.Round = WeaponAdditionalInfo.Round;
					//Remove !!! Debug
					myWeapon->ReloadTime = myWeaponInfo.ReloadTime;
					myWeapon->UpdateStateWeapon(MovementState);
					CurrentIndexWeapon = NewCurrentIndexWeapon;

					myWeapon->OnWeaponFireStart.AddDynamic(this, &AShooterGameCharacter::OnWeaponFireStart);
					myWeapon->OnWeaponReloadStart.AddDynamic(this, &AShooterGameCharacter::WeaponReloadStart);
					myWeapon->OnWeaponReloadEnd.AddDynamic(this, &AShooterGameCharacter::WeaponReloadEnd);

					if (CurrentWeapon->GetWeaponRound() <= 0 && CurrentWeapon->CheckCanWeaponReload())
						CurrentWeapon->InitReload();
					if (InventoryComponent)
						InventoryComponent->OnWeaponAmmoAviable.Broadcast(myWeapon->WeaponSetting.WeaponType);
				}
			}
		}
		else
		{
			UE_LOG(LogTemp, Warning, TEXT("AShooterGameCharacter::InitWeapon - Weapon not found in table -NULL"));
		}
	}


}

void AShooterGameCharacter::TryReloadWeapon()
{
	if (CurrentWeapon)
	{
		if (!CurrentWeapon->WeaponReloading)
			if (CurrentWeapon->GetWeaponRound() < CurrentWeapon->WeaponSetting.MaxRound && CurrentWeapon->CheckCanWeaponReload())
				CurrentWeapon->InitReload();

	}
}

void AShooterGameCharacter::RemoveCurrentWeapon()
{

}

void AShooterGameCharacter::WeaponReloadStart(UAnimMontage* Anim)
{
	WeaponReloadStart_BP(Anim);
	Walking = false;
	Aiming = false;
	Sprinting = false;
	ChangeMovementState();
}

void AShooterGameCharacter::OnWeaponFireStart(UAnimMontage* Anim)
{
	if (InventoryComponent && CurrentWeapon)
		InventoryComponent->SetAdditionalInfoWeapon(CurrentIndexWeapon, CurrentWeapon->WeaponInfo);
	OnWeaponFireStart_BP(Anim);
}

void AShooterGameCharacter::OnWeaponFireStart_BP_Implementation(UAnimMontage* Anim)
{
	// in BP
}

void AShooterGameCharacter::WeaponReloadEnd(bool bIsSuccess, int32 AmmoTake)
{
	if (InventoryComponent && CurrentWeapon)
	{
		InventoryComponent->AmmoSlotChangeValue(CurrentWeapon->WeaponSetting.WeaponType, AmmoTake);
		InventoryComponent->SetAdditionalInfoWeapon(CurrentIndexWeapon, CurrentWeapon->WeaponInfo);
		if ((CurrentWeapon->WeaponSetting.MaxRound - CurrentWeapon->WeaponInfo.Round) > AmmoTake)
			InventoryComponent->OnWeaponAmmoAviable.Broadcast(CurrentWeapon->WeaponSetting.WeaponType);
	}
	WeaponReloadEnd_BP(bIsSuccess);
	AWeaponDefault* myWeapon = nullptr;
	myWeapon = GetCurrentWeapon();
	if (myWeapon->WeaponFiring == true)
	{
		AttackCharEvent(true);
	}
}

bool AShooterGameCharacter::TrySwitchWeaponToIndexByKeyInput(int32 ToIndex)
{
	bool bIsSuccess = false;
	if (CurrentWeapon && !CurrentWeapon->WeaponReloading && InventoryComponent->WeaponSlots.IsValidIndex(ToIndex))
	{
		if (CurrentIndexWeapon != ToIndex && InventoryComponent)
		{
			int32 OldIndex = CurrentIndexWeapon;
			FAddicionalWeaponInfo OldInfo;

			if (CurrentWeapon)
			{
				OldInfo = CurrentWeapon->WeaponInfo;
				if (CurrentWeapon->WeaponReloading)
					CurrentWeapon->CancelReload();
			}

			bIsSuccess = InventoryComponent->SwitchWeaponByIndex(ToIndex, OldIndex, OldInfo);
		}
	}
	return bIsSuccess;
}

void AShooterGameCharacter::DropCurrentWeapon()
{
	if (InventoryComponent)
	{
		FDropItem ItemInfo;
		InventoryComponent->DropWeapobByIndex(CurrentIndexWeapon, ItemInfo);
	}
}

void AShooterGameCharacter::WeaponReloadStart_BP_Implementation(UAnimMontage* Anim)
{
	// in BP
}

void AShooterGameCharacter::WeaponReloadEnd_BP_Implementation(bool bIsSuccess)
{
	// in BP
}

void AShooterGameCharacter::TrySwicthNextWeapon()
{
	if (InventoryComponent->WeaponSlots.Num() > 1)
	{
		//We have more then one weapon go switch
		int8 OldIndex = CurrentIndexWeapon;
		FAddicionalWeaponInfo OldInfo;
		if (CurrentWeapon)
		{
			OldInfo = CurrentWeapon->WeaponInfo;
			if (CurrentWeapon->WeaponReloading)
				CurrentWeapon->CancelReload();
		}

		if (InventoryComponent)
		{
			InventoryComponent->SwitchWeaponToIndex(CurrentIndexWeapon + 1, OldIndex, OldInfo, true);
		}
	}
}

void AShooterGameCharacter::TrySwitchPreviosWeapon()
{
	if (InventoryComponent->WeaponSlots.Num() > 1)
	{
		//We have more then one weapon go switch
		int8 OldIndex = CurrentIndexWeapon;
		FAddicionalWeaponInfo OldInfo;
		if (CurrentWeapon)
		{
			OldInfo = CurrentWeapon->WeaponInfo;
			if (CurrentWeapon->WeaponReloading)
				CurrentWeapon->CancelReload();
		}

		if (InventoryComponent)
		{
			InventoryComponent->SwitchWeaponToIndex(CurrentIndexWeapon - 1, OldIndex, OldInfo, false);
		}
	}
}

void AShooterGameCharacter::TryAbilityEnabled()
{
	if (AbilityEffect)//TODO Cool down
	{
		UShooterGameStateEffect* NewEffect = NewObject<UShooterGameStateEffect>(this, AbilityEffect);
		if (NewEffect)
		{
			NewEffect->InitObject(this);
		}
	}
}

EPhysicalSurface AShooterGameCharacter::GetSurfuceType()
{
	EPhysicalSurface Result = EPhysicalSurface::SurfaceType_Default;
	if (CharHealthComponent)
	{
		if (CharHealthComponent->GetCurrentShield() <= 0)
		{
			if (GetMesh())
			{
				UMaterialInterface* myMaterial = GetMesh()->GetMaterial(0);
				if (myMaterial)
				{
					Result = myMaterial->GetPhysicalMaterial()->SurfaceType;
				}
			}
		}
	}
	return Result;
}

TArray<UShooterGameStateEffect*> AShooterGameCharacter::GetAllCurrentEffects()
{
	return Effects;
}

void AShooterGameCharacter::RemoveEffect(UShooterGameStateEffect* RemoveEffect)
{
	Effects.Remove(RemoveEffect);
}

void AShooterGameCharacter::AddEffect(UShooterGameStateEffect* newEffect)
{
	Effects.Add(newEffect);
}

void AShooterGameCharacter::CharDead()
{
	float TimeAnim = 0.0f;
	int32 rnd = FMath::RandHelper(DeadsAnim.Num());
	if (DeadsAnim.IsValidIndex(rnd) && DeadsAnim[rnd] && GetMesh() && GetMesh()->GetAnimInstance())
	{
		TimeAnim = DeadsAnim[rnd]->GetPlayLength();
		GetMesh()->GetAnimInstance()->Montage_Play(DeadsAnim[rnd]);
	}

	bIsAlive = false;

	UnPossessed();

	//Timer rag doll
	GetWorldTimerManager().SetTimer(TimerHandle_RagDollTimer, this, &AShooterGameCharacter::EnableRagdoll, TimeAnim, false);

	GetCursorToWorld()->SetVisibility(false);
}

void AShooterGameCharacter::EnableRagdoll()
{
	if (GetMesh())
	{
		GetMesh()->SetCollisionEnabled(ECollisionEnabled::PhysicsOnly);
		GetMesh()->SetSimulatePhysics(true);
	}
}

float AShooterGameCharacter::TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, AActor* DamageCauser)
{
	float ActualDamage = Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);
	if (bIsAlive)
	{
		CharHealthComponent->ChangeHealthValue(DamageAmount);
	}

	if (DamageEvent.IsOfType(FRadialDamageEvent::ClassID))
	{
		AProjectileDefault* myProjectile = Cast<AProjectileDefault>(DamageCauser);
		if (myProjectile)
		{
			UTypes::AddEffectBySurfaceType(this, myProjectile->ProjectileSetting.Effect, GetSurfuceType());
		}
	}

	return ActualDamage;
}
